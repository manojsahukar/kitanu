# Kitanu : Alternative Religion

> All hail to the god cell, maker of life.


> Good kitanu, bad kitanu. (Sura, Asura)


>Kitanu is everywhere, like the omnipresent idea of God.
    It is everywhere in your body, including the brain.

> You have free will, sort of. Kitanu's ecosystem in your body greatly influences your decision and emotion. So establishing the idea of God's will.

> Hell is sickness. Heaven is fitness. Hell and heaven are right here on the earth.

> Life started from the Kitanu. However, no one knows how the first Kitanu was born, just like the origin of God.

> When you die, Kitanu consumes you. That's the only afterlife you should really expect.

> Kitanu has all the answers of life. By understanding kitanu you can create a new kind of life.

> Prophet needs to have some prophecy. Probability can be used to assign values to the likelihood of future events. Derived from Probability, I'll call myself, Probet.

![Probet](probet.png)
![Probo](probo.png)

**While I'm probet, I'm not special. It's just I came before you, I happened before you. If you're probet `n`, I'm probet `n-1`. That's all.**

> You can be probet right now or when you choose, and when others want you in the community. 

```
Bible: Biotechnology, Thermodynamics of human.
Church: STEAM labs
```

```
Judgement Day: Biowarfare
Signs of Judgement Day: Antibiotics Resistance
```
Kitanu shows ways of life. There are many kinds of life, viz. Saprophytic, Symbiotic and Parasitic.
Child is a parasite like. Adult is independent. The faster the transition the better. Parasitic life is a sin.


> Confess your stupid mistakes. Get help from a real counselor. Psychological help, Financial help, Physical therapy.

`We prepare you for death, not eternal life.`

`We make you aware of your limits.`  
Magic tricks make you humble. Illusions are proof of brain failure.

# Rules
- Keep yourself clean.
- Keep your surroundings neat.
- Don't be poor.
- Don't be stupid.
- Eat healthily.
- Don't believe something without proof.
- Don't do anything that can make your life short.
- You're free to take your own life anytime.
- Don't break the code of coordination but be diplomatic.

Levels of ignorance are defined in Kitanu. By completing the milestone, one achieves better levels of ignorance. 

`Continuous learning is an important virtue of Probet.`

# Ref:
- https://www.researchgate.net/publication/272592057_Saprophytic_Symbiotic_and_Parasitic_Bacteria_Importance_to_Environment_Biotechnological_Applications_and_Biocontrol